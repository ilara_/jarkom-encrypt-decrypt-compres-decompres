﻿//Ilham Agung Riyadi
//4210181023

using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Text;
using System.Security.Cryptography;
using System.Runtime.InteropServices;
using System.IO.Compression;

namespace client_encrypt
{
    class Program
    {
        [DllImport("KERNEL32.DLL", EntryPoint = "RtlZeroMemory")]
        public static extern bool ZeroMemory(IntPtr Destination, int Length);

        //encrypt
        public static byte[] GenerateRandomSalt()
        {
            byte[] data = new byte[32];

            using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
            {
                for (int i = 0; i < 10; i++)
                {
                    // Fille the buffer with the generated data
                    rng.GetBytes(data);
                }
            }

            return data;
        }

        static void FileEncrypt(string inputFile, string password)
        {

            //generate random salt
            byte[] salt = GenerateRandomSalt();

            //create output file name
            FileStream fsCrypt = new FileStream(inputFile + ".aes", FileMode.Create);

            //convert password string to byte arrray
            byte[] passwordBytes = System.Text.Encoding.UTF8.GetBytes(password);

            //Set Rijndael symmetric encryption algorithm
            RijndaelManaged AES = new RijndaelManaged();
            AES.KeySize = 256;
            AES.BlockSize = 128;
            AES.Padding = PaddingMode.PKCS7;

            
            //"What it does is repeatedly hash the user password along with the salt." High iteration counts.
            var key = new Rfc2898DeriveBytes(passwordBytes, salt, 50000);
            AES.Key = key.GetBytes(AES.KeySize / 8);
            AES.IV = key.GetBytes(AES.BlockSize / 8);

            
            AES.Mode = CipherMode.CFB;

            // write salt to the begining of the output file, so in this case can be random every time
            fsCrypt.Write(salt, 0, salt.Length);

            CryptoStream cs = new CryptoStream(fsCrypt, AES.CreateEncryptor(), CryptoStreamMode.Write);

            FileStream fsIn = new FileStream(inputFile, FileMode.Open);

            //create a buffer (1mb) so only this amount will allocate in the memory and not the whole file
            byte[] buffer = new byte[1048576];
            int read;

            try
            {
                while ((read = fsIn.Read(buffer, 0, buffer.Length)) > 0)
                {
                   
                    cs.Write(buffer, 0, read);
                }

                // Close up
                fsIn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            finally
            {
                cs.Close();
                fsCrypt.Close();
            }
            Console.WriteLine("encrypt jalan");
        }

        //compress
        public static void Compress(DirectoryInfo directorySelected)
        {
            foreach (FileInfo fileToCompress in directorySelected.GetFiles())
            {
                using (FileStream originalFileStream = fileToCompress.OpenRead())
                {
                    if ((File.GetAttributes(fileToCompress.FullName) &
                       FileAttributes.Hidden) != FileAttributes.Hidden & fileToCompress.Extension != ".gz")
                    {
                        using (FileStream compressedFileStream = File.Create(fileToCompress.FullName + ".gz"))
                        {
                            using (GZipStream compressionStream = new GZipStream(compressedFileStream,
                               CompressionMode.Compress))
                            {
                                originalFileStream.CopyTo(compressionStream);

                            }
                        }
                        FileInfo info = new FileInfo(directoryPath + Path.DirectorySeparatorChar + fileToCompress.Name + ".gz");
                        Console.WriteLine($"Compressed {fileToCompress.Name} from {fileToCompress.Length.ToString()} to {info.Length.ToString()} bytes.");
                    }

                }
            }
            Console.WriteLine("compress jalan");
        }

        private static string directoryPath = @"D:\PENS\GT\MATKUL\semester3\client encrypt\client encrypt\bin\Debug\coba\";
        static void Main(string[] args)
        {
            try
            {
                // encrypt
                Console.WriteLine("Please enter a password to use:");
                string password = Console.ReadLine();
                Console.WriteLine("");

                // For additional security Pin the password of your files
                GCHandle gch = GCHandle.Alloc(password, GCHandleType.Pinned);

                Console.WriteLine("Please enter a full file path to encrypt:");
                string inputFile = Console.ReadLine();
                Console.WriteLine("");

                // Encrypt the file
                FileEncrypt(inputFile , password);
                Console.WriteLine("checked");

                // To increase the security of the encryption, delete the given password from the memory !
                ZeroMemory(gch.AddrOfPinnedObject(), password.Length * 2);
                gch.Free();

                Console.WriteLine(directoryPath);

                //compress
                DirectoryInfo directorySelected = new DirectoryInfo(directoryPath);
                
                Compress(directorySelected);

                TcpClient tcpClient = new TcpClient("127.0.0.1", 1234);  
                Console.WriteLine("Connected. Sending file.");  
 
                StreamWriter sWriter = new StreamWriter(tcpClient.GetStream());  
 
                byte[] bytes = File.ReadAllBytes(inputFile + ".aes" + ".gz");  
 
                sWriter.WriteLine(bytes.Length.ToString());  
                sWriter.Flush();  
 
                sWriter.WriteLine(inputFile + ".aes" + ".gz");  
                sWriter.Flush();  
 
                Console.WriteLine("Sending file");
                tcpClient.Client.SendFile(inputFile + ".aes" + ".gz");  
 
            }  
            catch (Exception e)  
            {  
                Console.Write(e.Message);  
            }  
 
            Console.Read();  
        }  
    }  
} 