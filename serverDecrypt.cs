﻿//Ilham Agung Riyadi
//4210181023

using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Text;
using System.Security.Cryptography;
using System.Runtime.InteropServices;
using System.IO.Compression;

namespace server_encrypt
{
    class serverDecrypt  
    {
        [DllImport("KERNEL32.DLL", EntryPoint = "RtlZeroMemory")]
        public static extern bool ZeroMemory(IntPtr Destination, int Length);

        //decompress
        public static void Decompress(FileInfo fileToDecompress)
        {
            using (FileStream originalFileStream = fileToDecompress.OpenRead())
            {
                string currentFileName = fileToDecompress.FullName;
                string newFileName = currentFileName.Remove(currentFileName.Length - fileToDecompress.Extension.Length);

                using (FileStream decompressedFileStream = File.Create(newFileName))
                {
                   
                    using (GZipStream decompressionStream = new GZipStream(originalFileStream, CompressionMode.Decompress))
                    {
                        
                        decompressionStream.CopyTo(decompressedFileStream);
                        Console.WriteLine($"Decompressed: {fileToDecompress.Name}");
                    }
                }
            }
            Console.WriteLine("decompress jalan");
        }

        //decrypt
        static void FileDecrypt(string inputFile, string outputFile, string password)
        {
            
            byte[] passwordBytes = System.Text.Encoding.UTF8.GetBytes(password);
            byte[] salt = new byte[32];

            FileStream fsCrypt = new FileStream(inputFile, FileMode.Open);
            
            fsCrypt.Read(salt, 0, salt.Length);

            RijndaelManaged AES = new RijndaelManaged();
            
            AES.KeySize = 256;
            AES.BlockSize = 128;
            var key = new Rfc2898DeriveBytes(passwordBytes, salt, 50000);
            AES.Key = key.GetBytes(AES.KeySize / 8);
            AES.IV = key.GetBytes(AES.BlockSize / 8);
            AES.Padding = PaddingMode.PKCS7;
            AES.Mode = CipherMode.CFB;

            CryptoStream cs = new CryptoStream(fsCrypt, AES.CreateDecryptor(), CryptoStreamMode.Read);
            
            FileStream fsOut = new FileStream(outputFile, FileMode.Create);
            
            int read;
            byte[] buffer = new byte[1048576];

            try
            {
                while ((read = cs.Read(buffer, 0, buffer.Length)) > 0)
                {
                  
                    fsOut.Write(buffer, 0, read);
                }
            }
            catch (CryptographicException ex_CryptographicException)
            {
                Console.WriteLine("CryptographicException error: " + ex_CryptographicException.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }

            try
            {
                cs.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error by closing CryptoStream: " + ex.Message);
            }
            finally
            {
                fsOut.Close();
                fsCrypt.Close();
            }
            Console.WriteLine("aman bos");
        }

        private static string directoryPath = @"D:\PENS\GT\MATKUL\semester3\server encrypt\server encrypt\bin\Debug\";
        static void Main(string[] args)  
        {  
            // Listen on port 1234    
            TcpListener tcpListener = new TcpListener(IPAddress.Any, 1234);  
            tcpListener.Start();  
 
            Console.WriteLine("Server started");  
 
            //Infinite loop to connect to new clients    
            while (true)  
            {  
                // Accept a TcpClient    
                TcpClient tcpClient = tcpListener.AcceptTcpClient();  
 
                Console.WriteLine("Connected to client");  
 
                StreamReader reader = new StreamReader(tcpClient.GetStream());  
 
                // The first message from the client is the file size    
                string cmdFileSize = reader.ReadLine();  
 
                // The first message from the client is the filename    
                string cmdFileName = reader.ReadLine();  
 
                int length = Convert.ToInt32(cmdFileSize);  
                byte[] buffer = new byte[length];  
                int received = 0;  
                int read = 0;  
                int size = 1024;  
                int remaining = 0;  
 
                // Read bytes from the client using the length sent from the client    
                while (received < length)  
                {  
                    remaining = length - received;  
                    if (remaining < size)  
                    {  
                        size = remaining;  
                    }  
 
                    read = tcpClient.GetStream().Read(buffer, received, size);  
                    received += read;  
                }

                // Save the file using the filename sent by the client    
                using (FileStream fStream = new FileStream(Path.GetFileName(cmdFileName), FileMode.Create))
                {
                    fStream.Write(buffer, 0, buffer.Length);
                    fStream.Flush();
                    fStream.Close();
                }

                Console.WriteLine("File received and saved in " + Environment.CurrentDirectory);

                Console.WriteLine(directoryPath);

                //decompress
                DirectoryInfo directorySelected = new DirectoryInfo(directoryPath);
                foreach (FileInfo fileToDecompress in directorySelected.GetFiles("*.gz"))
                {
                    Console.WriteLine(fileToDecompress);

                    Decompress(fileToDecompress);
                }

                Console.WriteLine("Please enter a password to use:");
                string password = Console.ReadLine();

                // For additional security Pin the password of your files
                GCHandle gch = GCHandle.Alloc(password, GCHandleType.Pinned);

                Console.WriteLine("Please enter a full file path to decrypt:");
                string inputFile = Console.ReadLine();
                Console.WriteLine("");

                Console.WriteLine("Please enter a full file path to put the decrypt:");
                string outputFile = Console.ReadLine();
                Console.WriteLine("");

                // Decrypt the file
                FileDecrypt(inputFile, outputFile, password);
                Console.WriteLine("checked");

                // To increase the security of the decryption, delete the used password from the memory !
                ZeroMemory(gch.AddrOfPinnedObject(), password.Length * 2);
                gch.Free();
 
            }  
        }  
    }  
} 
